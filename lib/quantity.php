<?php

function getIdStockAvailableAndSet($ProductId, $webService, $xml){    
    $opt['resource'] = 'products';
    $opt['id'] = $ProductId;
    $xml = $webService->get($opt);
    foreach ($xml->product->associations->stock_availables->stock_available as $item) {

       set_product_quantity($ProductId, $item->id, $item->id_product_attribute, $webService, $xml);
        }
    }


    function set_product_quantity($ProductId, $StokId, $AttributeId, $webService, $xml){
        $resources = $xml -> children() -> children();
        $resources->id = $StokId;
        $resources->id_product  = $ProductId;
        $resources->quantity = 10;
        $resources->id_shop = 1;
        $resources->out_of_stock=1;
        $resources->depends_on_stock = 0;
        $resources->id_product_attribute=$AttributeId;
        try {
            $opt = array('resource' => 'stock_availables');
            $opt['putXml'] = $xml->asXML();
            $opt['id'] = $StokId ;
            $webService->edit($opt);
        }catch (PrestaShopWebserviceException $ex) {
            echo "<b>Error </b>".$ex->getMessage().'<br>';
        }
}

?>