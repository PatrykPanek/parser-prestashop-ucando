<?php
require_once('simple_html_dom.php');


class ParseHTML {

    private $data;
    private $html;
    private $containers;
    private $imageContainers;


private function parser() {
    foreach($this->containers as $container) {
        $name = $container->find('a[class=transparent-link]');
        $prize = $container->find('span[class=c-price__current]');
        $shortDescriptionTemp = $container->find('ul[class=c-product-item__classifications]');
        $shortDescription  = '';
        $name[0]->innertext;
        $prize[0]->innertext;
        if(!array_key_exists(0, $shortDescriptionTemp)) $shortDescriptionTemp[0] = '';
        else {
            $temp = $shortDescriptionTemp[0]->find('li');  
            $tempString = '';
            foreach($temp as $t) {
                $tempString .= $t->innertext;
            }
            $shortDescriptionTemp[0] = $tempString;
        }
        $this->data[] = array('Name'=>$name[0]->innertext, 'Price'=>floatval($prize[0]->innertext),'ShortDescription'=>$shortDescriptionTemp[0]);
    }

    foreach($this->imageContainers as $key => $image) {
        $temp = $image->find('img[itemprop=image]');
        $this->data[$key]['Image'] = 'https://www.ucando.pl'.$temp[0]->src;
    }
}


    public function returnData() {
        return $this->data;
    }

    
    function __construct($url) {
        $this->html = new simple_html_dom();
        $this->html->load_file($url);
        $this->containers = $this->html->find('div[class=c-product-item__wrapper]');
        $this->imageContainers = $this->html->find('div[class=c-product-item__image]'); 
        $this->data = array();
        $this->parser();
    }

}

?>