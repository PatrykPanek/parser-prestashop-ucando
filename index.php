<style>
    html,
    body {
        margin: 0;
        padding: 0;
    }

    .container {
        max-width: 970px;
        margin-left: auto;
        margin-right: auto;
        padding-left: 15px;
        padding-right: 15px;
        width: 100%;
    }

    h1 {

        font-size: 20px;
        text-align: center;
    }

    form {
        text-align: center;
    }
</style>

<div class="container">
    <form method="GET" action="parser.php">
        <h1 >Wprowadź adres URL np <a href="https://www.ucando.pl/k/zawieszenia-i-amortyzatory/U03">https://www.ucando.pl/k/zawieszenia-i-amortyzatory/U03</a></h1>
        <br/>
        <input name="url" type="text">
        <p>Wprowadź kategorię do której będą przynależeć produkty</p>
        <input name="category" type="text">
        <br/>
        <input type="submit">
    </form>
</div>