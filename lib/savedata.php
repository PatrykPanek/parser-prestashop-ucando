<?php

require_once('addimage.php');
require_once('quantity.php');




class saveData {

    private $webService;
    private $xmlProduct;
    private $xmlQuantity;


   public function saveData($price, $name, $desc, $image, $category) {
        $opt = array('resource' => 'products');
       
        $resources = $this->xmlProduct->children()->children();

        $resources->price = $price;
        $resources->wholesale_price = $price;
        $resources->active = '1';
        $resources->available_for_order = '1';
        $resources->id_tax_rules_group = '1';
        $resources->show_price = '1';
        $resources->name->language[0][0] = $name;
        $resources->description->language[0][0] = '';
        $resources->description_short->language[0][0] = $desc;
        
        
        if(is_array($category)) {
            $categories = $resources->associations->addChild('categories'); 

            foreach ($category as $id_category) {
                $category = $categories->addChild('category');
                $category->addChild('id', $id_category);
            }

            $resources->associations->categories->category->id = end($category);
            $resources->id_category_default = end($category);


        } else {
            $resources->id_category_default = $category;
            $resources->associations->categories->category->id = $category;
        }

        

        $opt = array('resource' => 'products');
        $opt['postXml'] = $this->xmlProduct->asXML();
        
        $temp = $this->webService->add($opt); 
        echo "Dodano element!<br/>";

        image($image, $temp->product->id);
        echo "Dodano obrazek!<br/>";

        getIdStockAvailableAndSet($temp->product->id, $this->webService, $this->xmlQuantity);
        echo "Dodano ilość!<br/>";

        $temp = null;
       
    }

    function __construct($webService, $xmlProduct, $xmlQuantity) {
        $this->webService = $webService;
        $this->xmlProduct = $xmlProduct;
        $this->xmlQuantity = $xmlQuantity;
    }
}


?>